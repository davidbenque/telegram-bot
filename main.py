
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable=W0613, C0116
# type: ignore[union-attr]
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging
from random import choice

from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

from google_trans_new import google_translator

f = open('token.txt', 'r')
token = f.read()

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

# Translator
# https://github.com/ssut/py-googletrans/issues/234#issuecomment-722203541

t = google_translator()

# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')


def echo(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    update.message.reply_text("I got it:" + update.message.text)

def translate(update: Update, context: CallbackContext) -> None:
    text = update.message.text
    text = text[3:] # XXX Porky - TODO remove command with a regex
    detect = t.detect(text)
    lang_code = detect[0]
    input_langs = ['fr', 'en']

    if lang_code in input_langs:
        translate_text = t.translate(text, lang_tgt='ru')
    elif lang_code == 'ru':
        translate_text = t.translate(text, lang_tgt='fr')
    else:
        translate_text = "what you say to me?"

    update.message.reply_text(translate_text)

def question_anto(update: Update, context: CallbackContext) -> None:
    text = update.message.text

    prefix = ["it's", "the answer is", "ответ на это", "c'est"]
    suffix = ["✨💫", "🍆💦", "🤩", "of course"]
    answer = choice(prefix) + " Antonin " + choice(suffix)

    if text[-1] == "?":
        update.message.reply_text(answer)
    else:
        update.message.reply_text("If you want answers you must ask questions")

def question_random(update: Update, context: CallbackContext) -> None:
    text = update.message.text

    prefix = ["it's", "the answer is", "ответ на это", "c'est"]
    suffix = ["✨💫", "🍆💦", "🤩", "of course"]
    bifs = ['Alexandre', 'Antonin', 'Antoine', 'Nicolas', 'Pierre', 'Thomas', 'Julien', 'Marco', 'David', 'Roman', 'Raphael']
    answer = choice(prefix) + ' ' + choice(bifs) + ' ' + choice(suffix)

    if "?" in text:
        update.message.reply_text(answer)
    else:
        update.message.reply_text("If you want answers you must ask questions")

def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(token, use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("t", translate))
    dispatcher.add_handler(CommandHandler("q", question_anto))
    dispatcher.add_handler(CommandHandler("r", question_random))

    # on noncommand i.e message - echo the message on Telegram
    #dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()