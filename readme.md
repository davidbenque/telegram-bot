
## Installation

1) Install [Poetry](https://python-poetry.org/)
2) Clone this repo
3) `cd` into this repo
4) Run `poetry install`
5) Run `poetry shell` this will create a virtual environment the first time*
6) Get a token from @BotFather https://t.me/botfather
7) Put your token in a `token.txt` file in this repo
8) Inside the environment start the bot with `python main.py`

\* note the environment name and change the values in `.vscode/settings.json` to run in VSCode IPython shell.
  
## Google translate fix
2021-12-08 15:52:48

https://github.com/lushan88a/google_trans_new/issues/36

to avoid `json.decoder.JSONDecodeError: Extra data: `
edit lines 151 and 233 of the google_trans_new module

`/home/david/.cache/pypoetry/virtualenvs/telgram-bot-OIGBQxy_-py3.8/lib/python3.8/site-packages/google_trans_new/google_trans_new.py`

```diff
-  response = (decoded_line + ']')
+  response = (decoded_line)
```

## Tutorials/Resources

- https://github.com/python-telegram-bot/python-telegram-bot
    - https://github.com/python-telegram-bot/python-telegram-bot/wiki/Extensions-%E2%80%93-Your-first-Bot
- https://www.freecodecamp.org/news/learn-to-build-your-first-bot-in-telegram-with-python-4c99526765e4/
- https://www.toptal.com/python/telegram-bot-tutorial-python
- https://core.telegram.org/bots